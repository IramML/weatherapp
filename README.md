# WeatherAppAndroid
Weather app using OpenWeatherMap API with kotlin
## Screenshots
![Screenshot00](https://github.com/IramML/WeatherApp/blob/master/screenshots/Screenshot_20180712-003308.png)
![Screenshot01](https://github.com/IramML/WeatherApp/blob/master/screenshots/Screenshot_20180712-003255.png)
![Screenshot02](https://github.com/IramML/WeatherApp/blob/master/screenshots/Screenshot_20180712-003318.png)
